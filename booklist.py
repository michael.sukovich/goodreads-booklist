import requests
from bs4 import BeautifulSoup
import re


class Book:
    def __init__(self, title, author, rating):
        self.title = title
        self.author = author
        self.rating = rating
  
    # def __str__(self):
    #     return (f"{'*' * 30}\n{self.title} \n\tby: {self.author}\n with a rating of {self.rating}\n{'*'*30}\n")

    def get_books():
        site = requests.get("https://www.goodreads.com/list/show/32179.Best_Sci_Fi_Books_of_All_Time")
        dict_of_books = []
        soup = BeautifulSoup(site.content, 'html.parser')

        book_search = [book.text for book in soup.find_all(itemprop = "name")]

        titles = book_search[::2]
        authors = book_search[1::2]

        stars = [star.text for star in soup.find_all(class_="minirating")]

        books_list = list(zip(titles, authors, stars))


        for title in books_list:
            title = Book(title[0], title[1], title[2])
            dict_of_books.append(title)

        return dict_of_books

    def book_search(dict_of_books, search_title):
        # (books.title, books.author, books.rating)
        for book in dict_of_books:
            if search_title.lower() in book.title.lower():
                return (f"Search {search_title} found in {book.title} by {book.author}")

dict_of_books = Book.get_books()

print(Book.book_search(dict_of_books, "Lost"))

